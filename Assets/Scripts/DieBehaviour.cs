﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DieBehaviour : MonoBehaviour
{
    public Image dieImage;
    public List<Sprite> dieList = new List<Sprite>();
    public Button dieButton;
    public Button lifesButton;
    public Button colorButton;
    public Button exitButton;

    public void PlayDie()
    {
        StartCoroutine(DieAnimation());
    }

    private IEnumerator DieAnimation()
    {
        dieButton.interactable = false;
        lifesButton.interactable = false;
        colorButton.interactable = false;
        exitButton.interactable = false;
        RandomImage();
        yield return new WaitForSeconds(0.1f);
        RandomImage();
        yield return new WaitForSeconds(0.1f);
        RandomImage();
        yield return new WaitForSeconds(0.1f);
        RandomImage();
        yield return new WaitForSeconds(0.1f);
        RandomImage();
        yield return new WaitForSeconds(0.1f);
        RandomImage();
        yield return new WaitForSeconds(0.1f);
        RandomImage();
        yield return new WaitForSeconds(0.1f);
        RandomImage();
        yield return new WaitForSeconds(0.1f);
        RandomImage();
        yield return new WaitForSeconds(0.1f);
        RandomImage();
        yield return new WaitForSeconds(0.2f);
        RandomImage();
        yield return new WaitForSeconds(0.2f);
        RandomImage();
        yield return new WaitForSeconds(0.2f);
        RandomImage();
        yield return new WaitForSeconds(0.25f);
        RandomImage();
        yield return new WaitForSeconds(0.25f);
        RandomImage();
        yield return new WaitForSeconds(0.3f);
        RandomImage();
        yield return new WaitForSeconds(0.35f);
        RandomImage();
        yield return new WaitForSeconds(0.4f);
        RandomImage();
        dieButton.interactable = true;
        lifesButton.interactable = true;
        colorButton.interactable = true;
        exitButton.interactable = true;
    }

    private void RandomImage()
    {
        int random = Random.Range(0, 6);
        dieImage.sprite = dieList[random];
    }
}
