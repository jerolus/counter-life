﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLifesBehaviour : MonoBehaviour 
{
	public string id;
	public int counterLife = 20;
	public Text text;

	void Update()
	{
		text.text = counterLife.ToString();
	}

	public void LifeUp()
	{
		counterLife++;
	}

	public void LifeDown()
	{
		counterLife--;
	}

	public void LifeButton20()
	{
		counterLife = 20;
	}

	public void LifeButton40()
	{
		counterLife = 40;
	}

	public void LifeButton60()
	{
		counterLife = 60;
	}
}
