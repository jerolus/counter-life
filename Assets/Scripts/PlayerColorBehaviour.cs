﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerColorBehaviour : MonoBehaviour 
{
	public Text text;
	public Text textDown;
	public Text textUp;
	public Image background;
	public Image die;
	public Color red;
	public Color blue;
	public Color green;
	public Color black;
	public Color white;

	public void RedBackground()
	{
		text.color = Color.black;
		die.color = Color.black;
		textDown.color = Color.black;
		textUp.color = Color.black;
		background.color = red;
	}

	public void BlueBackground()
	{
		text.color = Color.black;
		die.color = Color.black;
		textDown.color = Color.black;
		textUp.color = Color.black;
		background.color = blue;
	}

	public void GreenBackground()
	{
		text.color = Color.black;
		die.color = Color.black;
		textDown.color = Color.black;
		textUp.color = Color.black;
		background.color = green;
	}

	public void WhiteBackground()
	{
		text.color = Color.black;
		die.color = Color.black;
		textDown.color = Color.black;
		textUp.color = Color.black;
		background.color = white;
	}

	public void BlackBackground()
	{
		text.color = Color.gray;
		die.color = Color.gray;
		textDown.color = Color.gray;
		textUp.color = Color.gray;
		background.color = black;
	}
}
